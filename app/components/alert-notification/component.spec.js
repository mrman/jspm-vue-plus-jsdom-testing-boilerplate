var should = require('should');
var TestUtil = require('../../../test/util');
var fixtures = require('../../../test/fixtures/alerts.js');
var JSPM = require('jspm');
var test = TestUtil.tape;

// Define the systemjs path to the component code
var COMPONENT_JS_PATH = 'app/components/alert-notification/component.js';

var Vue, Component;

test("alert-notification component", t => {

  // Import the component, using JSPM for uinit tests
  JSPM.import(COMPONENT_JS_PATH)
    .then(module => {
      t.ok(module.default.vue);
      t.ok(module.default.component);

      // Save Vue and component class to test-globals for later
      Vue = module.default.vue;
      Component = module.default.component;
    })
    .then(t.end)
    .catch(err => {
      console.log("Error occurred during setup:", err);
      t.fail(err);
    });
});

test("alert-notification component data is a function", t => {
  t.plan(1);
  t.assert( Component.data.should.have.type('function'), "Component data should be a function");
});

test("alert-notification component data spits out the correct fields and default values", t => {
  t.plan(2);

  var data = Component.data();
  t.assert( data.should.have.type('object'),  "Data function should return an object");
  t.assert( should(data.alert).be.undefined(), "Data.alert should be undefined initially");
});

test("alert-notification component has the default data values after instantiation (with no params)", t => {
  t.plan(1);
  var vm = new Vue(Component).$mount();
  t.assert( should(vm.alert).be.undefined(), "alert should be undefined");
});

test("alert-notification component has the correct status class when created", t => {
  t.plan(1);
  var vm = new Vue(Component).$mount();
  t.assert( vm.alertClass.trim().should.be.eql('alert'), "Alert class should be 'alert' if no message is specified");
});

test("alert-notification component properly server-side renders a single alert", t => {

  TestUtil.renderComponentWithData(Vue, Component, fixtures.ERROR)
    .then(w => {

      // Get the element and context
      var e = w.document.getElementsByClassName("alert-notification-component")[0];
      var content = e.textContent.trim();

      // Check content
      t.assert( content.should.not.be.empty(), "Content should be non-empty");
      t.assert( content.should.containEql('×'), "&times; should be rendered for closing notification");
      t.assert( content.should.containEql(fixtures.ERROR.propsData.alert.message), "Expect to see the alert message in the content");

      // Alert should be categorized properly
      t.assert( e.classList.should.matchAny("alert"), "classList should contain 'alert'");
      t.assert( e.classList.should.matchAny("alert-error"), "classList should contain 'alert-error'");

    })
    .then(t.end)
    .catch(TestUtil.failAndEnd(t));
});
