import AlertNotificationComponent from "app/components/alert-notification/component";
import Vue from "vue";
import t from "./main.html!vtc";

var app = new Vue({
  render: t.render,
  staticRenderFns: t.staticRenderFns,

  data: {
    // Alert that will be passed into an instance of <alert-notification/>
    alertToPassIn: {
      status: "success",
      message: "Everythings working out!"
    }
  },

  methods: {

    handleDestroy: function() {
      console.log("Would have handled destroy from alert-notification here!");
    }

  }
});

// Save app & vue instance to window for future use/inspection, mount app
window.App = app;
window.Vue = Vue;
app.$mount("#vue-app-container");
