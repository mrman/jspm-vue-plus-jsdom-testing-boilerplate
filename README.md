# JSPM + Vue.js + JSDOM boilerplate

This is a boilerplate to compliment the [blog series I wrote on testing vue components in a JSPM setup](https://vadosware.io/post/testing-dom-with-mocha-part-3/).

# Dependencies & related projects

0. [nodejs & npm](https://nodejs.org)
1. [JSPM](https://jspm.io)
2. [Vue.JS](https://vuejs.org/)
3. [tape](https://github.com/substack/tape)
4. [should](https://shouldjs.github.io/)
5. [jsdom](https://github.com/tmpvar/jsdom)
6. [vue-server-renderer](https://www.npmjs.com/package/vue-server-renderer)
8. [jspm-git](https://jspm.io)

# Quickstart

0. `clone` this repository 
1. `npm install`
2. `jspm install`
3. Install [systemjs-plugin-vue-template-compiler](https://gitlab.com/mrman/systemjs-plugin-vue-template-compiler) and other things required by it like `jspm-git` and the `gitlab` registry
4. Open `index.html` in your favorite web browser

What you *should* see is the the `alert-notification` component, rendered in all it's glory. If you don't see that at this point, you should go back and make sure to get that working. It's good to be able to at least see what component the tests are running on.

# Running the tests

## Run the unit tests for the component

`./node_modules/.bin/tape app/components/alert-notification/component.spec.js`

## Run the close-to-real-DOM JSDOM-powered end to end tests for the component

`./node_modules/.bin/tape app/components/alert-notification/component.spec.e2e.js`

# WTF is happening? 

This repo basically serves as an introduction to testing at almost every level in javascript. Sorry it's not a proper walkthrough and more of a code dump to illustrate some blog posts... Which are also not a proper walkthrough.

Check out the blog series I wrote on testing with JSPM, Vue.js, and `mocha` (which changed to `tape`), maybe it will help:

1. [Part 1 - Unit testing with JSPM, Mocha, and Vue.js](https://vadosware.io/post/testing-with-jspm-mocha-and-vuejs/)
2. [Part 2 - More testing with JSPM, Mocha, and Vue.js](https://vadosware.io/post/more-testing-with-jspm-mocha-and-vuejs/)
3. [Part 3 - Testing the DOM](https://vadosware.io/post/testing-dom-with-mocha-part-3/)
4. [Part 4 - Switching from mocha to tape](https://vadosware.io/post/switching-from-mocha-to-tape-and-testing-e2e/)

If you think this repo could be made better/more informative, file an issue!
