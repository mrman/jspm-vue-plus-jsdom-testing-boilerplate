
// Fixture for a simple alert @ init (prop)
exports.ERROR = {
  propsData: {
    alert: { status: "error", message: "This is a test alert"},
    alertIdx: 0
  }
};

// Create a single hint
exports.HINT_CREATION = {
  propsData: {
    hintCreateFn: function(h) {
      return new Promise.resolve(h);
    }
  }
};
