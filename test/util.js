var renderer = require('vue-server-renderer').createRenderer();
var jsdom = require('jsdom');
var JSPM = require('jspm');
var tape = require('tape');

/**
 * Render a component with data provided
 *
 * @param {Object} Vue - The vue instance to use to render the component
 * @param {Object} Component - The component to render
 * @param {Object} props - The data to feed in to the component at init
 */
exports.renderComponentWithData = function(Vue, Component, props) {
  // Create component and mount
  var ModifiedVue = Vue.extend(Component);
  var vm = new ModifiedVue(props || {});

  return new Promise(function(resolve, reject) {
    // Render the component
    renderer.renderToString(vm, function(err, html) {
      if (err) reject(err);

      // Start up JSDOM and check the element
      jsdom.env(html, function(err, window) {
        if (err) reject(err);


        // Resolve window object
        resolve(window);
      });
    });
  });
};

/**
 * Render a module (usually a component) in JSDOM, by:
 * - Creating a JSPM builder
 * - Building a static bundle of the module
 * - Loading the bundle with JSDOM
 *
 * @param {string} path - Path to the module code
 * @param {string} name - The name to use for the name of the exported module
 * @param {string} [builder] - JSPM builder to re-use
 * @returns A promise that resolves with the window of the generated jsdom environment
 */
function renderModuleInJSDOM(path, name, builder) {
  // Create builder if one wasn't provided
  if (typeof builder === "undefined") {
    builder = new JSPM.Builder();
  }

  return new Promise(function(resolve, reject) {

    // Make a static build of th module
    builder.buildStatic(path, { minify: true, globalName: name })
      .then(output => {

        // Load the module and render an empty HTML page
        jsdom.env({ html: '', src: [output.source], done: function(err, window) {
          if (err) reject(err);
          resolve(window);
        }});

      });

  });

};
exports.renderModuleInJSDOM = renderModuleInJSDOM;

/**
 * Instantiate a component into an off-page element from a loaded global module in JSDOM
 * The component module is expected to have `vue` and `component` properties.
 *
 * @param {string} globalName - The name of the loaded component module.
 * @param {Object} data - Data to pass to the component on initialization
 * @param {Object} data.propsData - Initial props for the component
 * @returns A function that operates on a window, and resolves to original window, the vm, and element or the relevant component
 */
function instantiateComponentFromLoadedGlobal(globalName, data) {
  return function(window) {

    if (typeof window[globalName] === "undefined" || window[globalName] == null) {
      throw new Error("Incorrect/Invalid global name, failed to find window[" + globalName + "]");
    }

    // Grab the loaded element, component, and vue fn
    var vueFn  = window[globalName].default.vue;
    var comp  = vueFn.extend(window[globalName].default.component);

    // Create and mount component into off-page element
    var vm = (new comp(data)).$mount();
    var elem = vm.$el;

    // NOTE - the element is NOT on the page at this point, it's off-page
    // The structure below is referred to as an ElemInfo object frequently.
    return {vm, elem, vueFn, comp, window};
  };
}

exports.instantiateComponentFromLoadedGlobal = instantiateComponentFromLoadedGlobal;

function isValidElemInfoObj(obj) {
  return obj.vm && obj.elem && obj.window && obj.comp;
}

/**
 * Add component's element (expeted to be off-page) to the body of the HTML document. Usually called after `instantiateComponentFromLoadedGlobal`
 *
 * @param {ElemInfo} info - Information about element/env see `instantiateComponentFromLoadedGlobal`
 * @returns A an object (elemInfo) that contains the window, the vm, and element or the relevant component. see `instantiateComponentFromLoadedGlobal`
 */
function addComponentElementToBody(info) {
  if (!isValidElemInfoObj(info)) throw new Error("Invalid ElemInfo object:", info);

  // Add elem to document body
  info.window
    .document
    .documentElement
    .getElementsByTagName("body")[0]
    .appendChild(info.elem);

  // NOTE - the element is NOT on the page at this point, it's off-page
  return info;
}

exports.addComponentElementToBody = addComponentElementToBody;

/**
 * Simulate a click in JSDOM
 *
 * @param {EventTarget} tgt - The element that should be the target for the click
 * @param {Window} window - The JSDOM-provided window of the page
 * @param {object} options - Click event options (by default an cancelable, event that bubbles, with the given element as the view)
 * @param {Function} tgt.dispatchEvent - The element should be able to dispatch events.
 */
function jsdomClick(tgt, window, options) {
  tgt.dispatchEvent(
    new window.MouseEvent("click", options || { bubbles: true, cancelable: true, view: tgt })
  );
}
exports.jsdomClick = jsdomClick;

/**
 * Fail and end a test as provided by tape.
 *
 * @param {Object} t - Test to fail and end
 * @param {Function} t.fail - Should cause a failing assertion
 * @param {Function} t.fail - Should cause a failing assertion
 * @param {string} msg - Failure message
 * @returns A function that ignores it's input, calls .fail then .end on the given t
 */
function failAndEnd(t, msg) {
  return function(err) {
    t.fail(msg);
    t.end(err);
  };
}
exports.failAndEnd = failAndEnd;

tape.onFinish(() => process.exit(0));
exports.tape = tape;
